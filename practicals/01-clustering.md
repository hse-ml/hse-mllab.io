<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2021 Nick Howell

(how to read this document: https://commonmark.org/help/)
-->

# K-means clustering practical

These tasks use *type annotations* to describe the types of inputs and
outputs of functions; read about them in the [Python documentation on
type annotations].

[Python documentation on type annotations]: https://docs.python.org/3/library/typing.html

They also use `numpy`; I'm assuming some basic familiarity with it.

Tasks:
1. Write a clustering problem generator with signature:
```python
def scatter_clusters(
  centers: ArrayLike,
  spread: ArrayLike,
  n_points: int
) -> ArrayLike:
```

For k=3, generate easy and hard problems and plot them; the easy
problem might look like figure 3.13 from Daumé.

2. Implement K-means clustering as shown in Daumé:
```python
def kmeans_cluster_assignment(
  k: int,
  points: ArrayLike,
  centers_guess: Optional[ArrayLike] = None,
  max_iterations: Optional[int] = None,
  tolerance: Optional[float] = None
) -> ArrayLike:
```

Replot your problems at 5 stages (random initialisation, 25%, 50%,
75%, 100% of iterations), using colours to assign points to clusters.

The easy problem plots might look like the coloured plots in figure
3.14 from Daumé.

3. Study the performance of these two implementations: memory, speed, quality; compare against
   [scipy.cluster.vq.kmeans].

[scipy.cluster.vq.kmeans]: https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.vq.kmeans.html


4. Compute the performance of your algorithm as percent of points
   assigned the correct cluster. (Your algorithm may order the
   clusters differently!) Graph this as a function of iterations, at
   10% intervals.

   Make a random 10-90 test-train split; now you train on 90% of the
   data, and evaluate on the other 10%. How does the performance graph
   change?

5. Instead of a pure 10-90 split, divide your data into 10 portions.
   Picking one of these portions as test and the rest as train, we have 10 different 10-90 test-train splits. Each split gives a different train-eval run, and thus a different performance number.

   Perform *cross-validation* on your training data: plot the mean of
   these performances against percent-of-iterations. Error bars for
   these means are computed using standard deviation. Use filled
   plotting to show this region on the graph with
   `matplotlib.pyplot.fill_between`.
