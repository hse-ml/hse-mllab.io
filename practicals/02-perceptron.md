<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2021 Nick Howell

(how to read this document: https://commonmark.org/help/)
-->

# Perceptron practical

For this practical, numpy and other numerical libraries are forbidden.
You may use only [Python standard libraries] and code you write and
submit yourself.

[Python standard libraries]: https://docs.python.org/3/library/index.html

Tasks:

1. Implement your own `Scalar` and `Vector` classes, without using
   any other modules:
```python
from typing import Union, List
from math import sqrt
class Scalar:
  pass
class Vector:
  pass

class Scalar:
  def __init__(self: Scalar, val: float):
    self.val = float(val)
  def __mul__(self: Scalar, other: Union[Scalar, Vector]) -> Union[Scalar, Vector]:
    # hint: use isinstance to decide what `other` is
    # raise an error if `other` isn't Scalar or Vector!
    pass
  def __add__(self: Scalar, other: Scalar) -> Scalar:
    pass
  def __sub__(self: Scalar, other: Scalar) -> Scalar:
    pass
  def __truediv__(self: Scalar, other: Scalar) -> Scalar:
    pass # implement division of scalars
  def __rtruediv__(self: Scalar, other: Vector) -> Vector:
    pass # implement division of vector by scalar
  def __repr__(self: Scalar) -> str:
    return "Scalar(%r)" % self.val
  def sign(self: Scalar) -> int:
    pass # returns -1, 0, or 1
  def __float__(self: Scalar) -> float:
    return self.val

class Vector:
  def __init__(self: Vector, *entries: List[float]):
    self.entries = entries
  def zero(size: int) -> Vector:
    return Vector(*[0 for i in range(size)])
  def __add__(self: Vector, other: Vector) -> Vector:
    pass
  def __sub__(self: Vector, other: Vector) -> Vector:
    return self + (-1)*other
  def __mul__(self: Vector, other: Vector) -> Scalar:
    pass
  def magnitude(self: Vector) -> Scalar:
    pass
  def unit(self: Vector) -> Vector:
    return self / self.magnitude()
  def __len__(self: Vector) -> int:
    return len(self.entries)
  def __repr__(self: Vector) -> str:
    return "Vector%s" % repr(self.entries)
  def __iter__(self: Vector):
    return iter(self.entries)
```

2. Implement the `PerceptronTrain` and `PerceptronTest` functions,
   using your `Vector` and `Scalar` classes. Do not permute the
   dataset when training; run through it linearly.

  (Hint on how to use the classes: make `w` and `x` instances of
  `Vector`, `y` and `b` instances of `Scalar`. What should the type of
  `D` be? Where do you see the vector operation formulas?)
  

3. Make a 90-10 test-train split and evaluate your algorithm on the
   following dataset:
```
from random import randint
v = Vector(randint(-100, 100), randint(-100, 100))
xs = [Vector(randint(-100, 100), randint(-100, 100)) for i in range(500)]
ys = [v * x * Scalar(randint(-1, 9)) for x in xs]
```

   You should get that `w` is some multiple of `v`, and the
   performance should be very good. (Some noise is introduced by the
   last factor in `y`.)

4. Make a 90-10 test-train split and evaluate your algorithm on the
   `xor` dataset:
```
from random import randint
xs = [Vector(randint(-100, 100), randint(-100, 100)) for i in range(500)]
ys = [Scalar(1) if x.entries[0]*x.entries[1] < 0 else -1 for x in xs]
```

   You should get some relatively random `w`, and the performance
   should be terrible.

5. Sort the training data from task 3 so that all samples with `y < 0`
   come first, then all samples with `y = 0`, then all samples with
   `y > 0`.  (That is, sort by `y`.)

   Graph the performance (computed by `PerceptronTest`) on both train
   and test sets versus epochs for perceptrons trained on
   - no permutation
   - random permutation at the beginning
   - random permutation at each epoch

   (This replicates graph 4.4 from Daumé.)

6. Implement `AveragedPerceptronTrain`; using random permutation at
   each epoch, compare its performance with `PerceptronTrain` using
   the dataset from task 3.

