<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2021 Nick Howell

(how to read this document: https://commonmark.org/help/)
-->

# Decision trees practical

Tasks:
1. Use the `Tree` data structure below; write code to build the tree
   from figure 1.2 in Daumé.

```python
class Tree:
  '''Create a binary tree; keyword-only arguments `data`, `left`, `right`.

  Examples:
    l1 = Tree.leaf("leaf1")
    l2 = Tree.leaf("leaf2")
    tree = Tree(data="root", left=l1, right=Tree(right=l2))
  '''

  def leaf(data):
    '''Create a leaf tree
    '''
    return Tree(data=data)

  # pretty-print trees
  def __repr__(self):
    if self.is_leaf():
      return "Leaf(%r)" % self.data
    else:
      return "Tree(%r) { left = %r, right = %r }" % (self.data, self.left, self.right) 

  # all arguments after `*` are *keyword-only*!
  def __init__(self, *, data = None, left = None, right = None):
    self.data = data
    self.left = left
    self.right = right

  def is_leaf(self):
    '''Check if this tree is a leaf tree
    '''
    return self.left == None and self.right == None

  def children(self):
    '''List of child subtrees
    '''
    return [x for x in [self.left, self.right] if x]

  def depth(self):
    '''Compute the depth of a tree
    A leaf is depth-1, and a child is one deeper than the parent.
    '''
    return max([x.depth() for x in self.children()], default=0) + 1

```

2. In your python code, load the following dataset and add a boolean
   "ok" column, where "True" means the rating is non-negative and
   "False" means the rating is negative.

```csv
rating,easy,ai,systems,theory,morning
 2,True,True,False,True,False
 2,True,True,False,True,False
 2,False,True,False,False,False
 2,False,False,False,True,False
 2,False,True,True,False,True
 1,True,True,False,False,False
 1,True,True,False,True,False
 1,False,True,False,True,False
 0,False,False,False,False,True
 0,True,False,False,True,True
 0,False,True,False,True,False
 0,True,True,True,True,True
-1,True,True,True,False,True
-1,False,False,True,True,False
-1,False,False,True,False,True
-1,True,False,True,False,True
-2,False,False,True,True,False
-2,False,True,True,False,True
-2,True,False,True,False,False
-2,True,False,True,False,True
```

3. Write a function which takes a feature and computes the performance
   of the corresponding single-feature classifier:
```python
def single_feature_score(data, goal, feature):
  pass # replace this!
```
   
   Use this to find the best feature:
```python
def best_feature(data, goal, features):
  # optional: avoid the lambda using `functools.partial`
  return max(features, key=lambda f: single_feature_score(data, goal, f))
```
   Which feature is best? Which feature is worst?

4. Implement the `DecisionTreeTrain` and `DecisionTreeTest` algorithms
   from Daumé, returning `Tree`s. (Note: our dataset and his are
   different; we won't get the same tree.)

   How does the performance compare to the single-feature classifiers?

5. Add an optional `maxdepth` parameter to `DecisionTreeTrain`, which
   limits the depth of the tree produced. Plot performance against
   `maxdepth`.

