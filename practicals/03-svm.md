<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2021 Nick Howell

(how to read this document: https://commonmark.org/help/)
-->

# Support vector machines practical

For this practical, [numpy], [scipy.optimize.minimize], and
[matplotlib.pyplot] may be used.
You may use any [Python standard libraries] that you like.

[Python standard libraries]: https://docs.python.org/3/library/index.html
[numpy]: https://numpy.org/doc/stable/reference/index.html
[scipy.optimize.minimize]: https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html
[matplotlib.pyplot]: https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.html

Tasks:

1. Read the documentation for `scipy.optimize.minimize`, paying
   special attention to the Jacobian argument `jac`. Who computes the
   gradient, the `minimize` function itself, or the developer using
   it?

   Run the following two examples; which performs better?

Example:
```python
import scipy.optimize, numpy.random
def f(x):
  return x**2

def df(x):
  return 2*x

print(scipy.optimize.minimize(f, numpy.random.randint(-1000, 1000), jac=df))
```

Example:
```python
import scipy.optimize, numpy.random
def f(x):
  return x**2

print(scipy.optimize.minimize(f, numpy.random.randint(-1000, 1000), jac=False))
```

2. Write in python the loss function for support vector machines from
   equation (7.48) of Daumé. You can use the following hinge loss
   surrogate:

```python
def hinge_loss_surrogate(y_gold, y_pred):
  return numpy.max(0, 1 - y_gold * y_pred)

def svm_loss(w, b, C, D):
  # replace with your implementation, must call hinge_loss_surrogate
  pass
```

3. Use `scipy.optimize.minimize` with `jac=False` to implement support
   vector machines.

```python
def svm(D):
  # compute w and b with scipy.optimize.minimize and return them
  pass
```

4. Implement the gradient of `svm_loss`, and add an optional flag to
   `svm` to use it:
```python
def gradient_hinge_loss_surrogate(y_gold, y_pred):
  if hinge_loss_surrogate(y_gold, y_pred) == 0:
    return [0, 0]
  else:
    return [-y_pred, -y_gold]

def gradient_svm_loss(w, b, C, D):
  # implement the gradient of svm_loss
  pass

def svm(D, use_gradient=False):
  # use either jac=False or jac=gradient_svm_loss in
  # scipy.optimize.minimize
  pass
```

5. Use `numpy.random.normal` to generate two isolated clusters of
   points in 2 dimensions, one `x_plus` and one `x_minus`, and graph
   the three hyperplanes found by training:
   * an averaged perceptron
   * support vector machine without gradient
   * support vector machine with gradient.

   How do they compare?

```python
x_plus = numpy.random.normal(loc=[-1,-1], scale=0.5, size=(20,2))
x_minus = numpy.random.normal(loc=[1,1], scale=0.5, size=(20,2))

matplotlib.pyplot.scatter(
	x_plus[:,0], x_plus[:,1],
	marker='+',
	color='blue'
)
matplotlib.pyplot.scatter(
	x_minus[:,0], x_minus[:,1],
	marker='x',
	color='red'
)

# train perceptron and the two SVMs

# plot the hyperplanes they find

# save as svm-svm-perceptron.pdf
matplotlib.pyplot.savefig("svm-svm-perceptron.pdf") 
```
