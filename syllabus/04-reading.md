# Reading Assignments

Readings will come primarily from [A Course in Machine Learning (Daumé
)](https://ciml.info); supplementary material may be drawn from a
number of sources. Time permitting, we will continue our discussion of
neural networks with Neural Network Methods for Natural Language
Processing (Goldberg, 2017).

Here is an aspirational schedule:
```
*Week*	*Reading*
11.01	Introduction
18.01	Daumé ch. 1-2
25.01	Daumé ch. 3-4
01.02	Daumé ch. 5
08.02	Daumé ch. 6
15.02	Daumé ch. 7
22.02	Daumé ch. 8
01.03	Daumé ch. 9
08.03	Daumé ch. 11
22.03	Daumé ch. 12-13
29.03	Daumé ch. 13
05.04	Daumé ch. 14
12.04	Daumé ch. 15
19.04	Daumé ch. 15
26.04	Daumé ch. 16
03.05	Holidays
10.05	Daumé ch. 17
17.05	Daumé ch. 18
24.05	Goldberg ch. 4
31.05	Goldberg ch. 4
07.06	Goldberg ch. 5
14.06	Goldberg ch. 5
```
