# Grading

Grades for each task are given with maximum score 2: the rubrick is:
0. no serious attempt to respond
1. many or significant errors
2. no more than a few trivial errors

The following types of tasks will be assigned:
* practical, exploring an ML algorithm
* peer review exchange with IU
* collaborative project with IU

Grade distribution is: 60% practicals, 10% peer review, 30%
collaborative project.

## Practicals

Roughly every three weeks, you will implement an algorithm from our
readings, and write a Markdown-format (i.e., plaintext) blog post
explaining it.

You are encouraged to collaborate and help each other, but each
student must submit their own work.

Here's a tentative list of practicals:
1. K-means clustering
2. Perceptron
3. Support vector machines
4. Naïve Bayes
5. Neural Networks
6. Kernelisation
7. Ensemble methods

## Peer review

Roughly every three weeks, you will exchange peer-review feedback with
your IU collaborator: you will review their work, they will review
your work.

You must submit to me:
* your review of their work
* their review of your work
* your response to their review

You may improve and resubmit your practical based on their review; I
am happy to change your grade based on this.

## Collaborative project

During weeks 3 -- 15 you will work in groups together with students
from Indiana University to solve a problem in NLP.

Groups and projects will be pre-assigned; please express your [project
preference].

[project preference]: https://dudle.inf.tu-dresden.de/iu-hse-global-classroom-project/

Here is the rough schedule for the projects:

```
*Week*  *Project event*
25.01   Introductions
01.02   Groups formed
-- 8 weeks later --
12.04   Submission
19.04   Peer review
26.04   Presentations
```
