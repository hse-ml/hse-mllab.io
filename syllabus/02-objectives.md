# Learning Objectives

Students will be able to interpret, implement, and evaluate machine
learning algorithms for a wide range of tasks, and apply these skills
in international collaboration solving problems in NLP.

