# Learning Outcomes

At the end of the course, students will
- be able to formulate abstract problems quantitatively as learning
  tasks
- be able to weigh various models selecting an appropriate one for a
  given task
- have implemented these models, and be aware of industry-standard
  reference implementations
- have evaluated the performance of models and the design of
  experiments
- gained experience working in international collaborations

