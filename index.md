<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2021 Nick Howell

(how to read this document: https://commonmark.org/help/)
-->

# Introduction to Machine Learning

This is the (under-construction) home page of the [HSE] [School of
Linguistics] course [Introduction to Machine Learning]. Here you
will find information about:
* instructors
* meetings (time, place, readings)
* practicals

[Syllabus](syllabus.html) - basic information about the class.

To download a plain-text copy of this site, clone
[https://gitlab.com/hse-ml/hse-ml.gitlab.io.git](https://gitlab.com/hse-ml/hse-ml.gitlab.io.git).

<div style="column-width:30em">

## Instructors
I'm Nick Howell, assistant professor at the [School of Linguistics].
My PGP fingerprint is A59F2630 93CB492B 4479EDAE 6E855DEE 48E8A9D1 
([key](A59F263093CB492B4479EDAE6E855DEE48E8A9D1.asc)). You can reach
me by e-mail at nlhowell at gmail dot com.

## Meetings

The group will meet by Zoom and Telegram:
* Wed 2021-04-21 18.10-21
* Sat 2021-05-15 11.10-14
* Wed 2021-05-19 18.10-21
* Sat 2021-05-29 11.10-14

Rough reading schedule:
```tsv
11.01	Introduction
18.01	Daumé ch. 1-2
01.02	Daumé ch. 3
15.02	Daumé ch. 3
01.03	Daumé ch. 4
22.03	Daumé ch. 5-6
19.04	Daumé ch. 6-7
26.04	Daumé ch. 8
03.05	Holidays
17.05	Daumé ch. 9, 13?
```

## Practicals

Here's a tentative list of practicals:
1. [Decision trees]
2. [K-means clustering]
3. [Perceptron]
4. [Support vector machines]
5. Naïve Bayes
6. Neural Networks
7. Kernelisation
8. Ensemble methods

[Decision trees]: practicals/00-trees.html 
[K-means clustering]: practicals/01-clustering.html
[Perceptron]: practicals/02-perceptron.html
[Support vector machines]: practicals/03-svm.html

## Projects

Here is the rough schedule for the [projects]:

```
* 18.01   Test baselines, submit preferences
* 25.01   Introductions
* 01.02   Groups formed
* 08.02   Literature review (list of papers)
* 15.02   Literature review (written summary)
* 22.02   --
* 01.03   --
* 08.03   --
* 15.03   --
* 22.03   --
* 29.03   --
  04.04   Test set released
* 05.04   
  07.04   Submission of results
* 12.04   
* 16.04   Submission of paper (via EasyChair)
* 19.04   Peer review
  23.04   Submission of peer review
* 27.04   Presentations
  30.04   Submission of camera-ready
```

Presentation slots:
```
Slot    Date      Moscow       Indiana
01	27.04	23:15-23:35  15:15-15:35
02	27.04	23:35-23:55  15:35-15:55
03	28.04	18:10-18:30  10:10-10:30
04	28.04	18:30-18:50  10:30-10:50
05	28.04	18:50-19:10  10:50-11:10
06	28.04	19:10-19:30  11:10-11:30
07	28.04	19:30-19:50  11:30-11:50
08	28.04	19:50-20:10  11:50-12:10
09	28.04	20:10-20:30  12:10-12:30
10	28.04	20:30-20:50  12:30-12:50
11	29.04	23:15-23:35  15:15-15:35
12	29.04	23:35-23:55  15:35-15:55
```

[projects]: https://github.com/ftyers/global-classroom

</div>

[HSE]: https://hse.ru
[School of Linguistics]: https://ling.hse.ru
[Introduction to Machine Learning]: https://hse-ml.gitlab.io
